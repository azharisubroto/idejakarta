<?php
/**
 * Understrap enqueue scripts
 *
 * @package understrap
 */

if ( ! function_exists( 'understrap_scripts' ) ) {
	/**
	 * Load theme's JavaScript sources.
	 */
	function understrap_scripts() {
		// Get the theme data.
		$the_theme = wp_get_theme();
		wp_enqueue_style( 'understrap-styles', get_stylesheet_directory_uri() . '/css/theme.min.css', array(), $the_theme->get( 'Version' ), false );
		wp_deregister_script('jquery');
			wp_register_script('jquery', (get_stylesheet_directory_uri() . '/js/jquery-3.2.1.min.js'), false, '3.2.1');
			wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), true);
		if( is_single() ) {
			wp_enqueue_script( 'ide-social', get_template_directory_uri() . '/js/social.js', array(), true);
		}
		wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/js/theme.min.js', array(), $the_theme->get( 'Version' ), true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		wp_localize_script( 'understrap-scripts', 'simpleLikes', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'like' => esc_html__( 'Like', 'understrap' ),
			'unlike' => esc_html__( 'Unlike', 'understrap' )
		) );

	}
} // endif function_exists( 'understrap_scripts' ).

add_action( 'wp_enqueue_scripts', 'understrap_scripts' );

function adminscriptjago($hook) {
    if ('post.php' != $hook) {
        return;
    }
    wp_enqueue_script('idejak-gmaps-api', 'https://maps.google.com/maps/api/js?key=AIzaSyCXWyACfQiUcC_obK0en64L-rh8JioP4qE', array(), true);
    wp_enqueue_script('idejak-gmaps', get_template_directory_uri() . '/src/js/gmaps.min.js', array(), true);
    wp_enqueue_script('idejak-custom-admin', get_template_directory_uri() . '/src/js/custom-admin.js', array(), true);
}
add_action('admin_enqueue_scripts', 'adminscriptjago');