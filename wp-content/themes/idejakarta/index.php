<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

<div class="wrapper" id="wrapper-index">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check and opens the primary div -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<?php if (is_user_logged_in() == true) { ?>
						<li class="nav-item">
							<a class="nav-link active" id="idesaya-tab" data-toggle="tab" href="#idesaya" role="tab" aria-controls="idesaya" aria-selected="true">Ide Saya</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="onproses-tab" data-toggle="tab" href="#onproses" role="tab" aria-controls="onproses" aria-selected="false">Ide dari semua pengguna</a>
						</li>
					<?php } else { ?>
						<li class="nav-item">
							<a class="nav-link active" id="onproses-tab" data-toggle="tab" href="#onproses" role="tab" aria-controls="onproses" aria-selected="true">Ide dari semua pengguna</a>
						</li>
					<?php } ?>
					
					<!-- <li class="nav-item">
						<a class="nav-link" id="approved-tab" data-toggle="tab" href="#approved" role="tab" aria-controls="approved" aria-selected="false">Ide Disetujui</a>
					</li> -->
				</ul>
				<div class="tab-content card" id="myTabContent">
					<?php if (is_user_logged_in() == true) { ?>
					<div class="tab-pane fade show active" id="idesaya" role="tabpanel" aria-labelledby="idesaya-tab">
						
						<ul class="main-list-ide">
							<?php 
								$args = array(
								    'author'        =>  get_current_user_id(),
								    'orderby'       =>  'post_date',
								    'order'         =>  'DESC',
								    'posts_per_page' => 999,
								    'post_status' => array('publish', 'pending', 'private')
								);
								$query = new WP_Query($args);
								while( $query->have_posts() ): $query->the_post();
							?>
								<li>
									<div class="row">
										<div class="col-md-3">
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</div>
										<div class="col-md-2">
											By: <?php the_author_posts_link(); ?>
										</div>
										<div class="col-md-3">
											<p><?php echo bd_excerpt( 10 ); ?></p>
										</div>
										<div class="col-md-4">
											<?php if(function_exists('status_app')) status_app(get_the_ID()); ?>
										</div>
									</div>

									<?php /*
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
									<div class="clearfix"></div>
									Status: <?php the_field( "progress", get_the_ID() ); ?> <br>
									Approval: <?php the_field( "approval", get_the_ID() ); ?>
									*/ ?>
								</li>
							<?php
								endwhile;
								wp_reset_postdata();
							 ?>
						 </ul >
					</div>
					<div class="tab-pane fade show" id="onproses" role="tabpanel" aria-labelledby="onproses-tab">
						<ul class="main-list-ide">
							<?php 
								$args = array(
								    'orderby'       =>  'post_date',
								    'order'         =>  'DESC',
								    'posts_per_page' => 999,
								    'post_status' => array('publish', 'pending', 'private')
								);
								$query = new WP_Query($args);
								while( $query->have_posts() ): $query->the_post();
							?>
								<li>
									<div class="row">
										<div class="col-md-3">
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</div>
										<div class="col-md-2">
											By: <?php the_author_posts_link(); ?>
										</div>
										<div class="col-md-3">
											<p><?php echo bd_excerpt( 10 ); ?></p>
										</div>
										<div class="col-md-4">
											<?php if(function_exists('status_app')) status_app(get_the_ID()); ?>
										</div>
									</div>
								</li>
							<?php
								endwhile;
								wp_reset_postdata();
							 ?>
						 </ul >
					</div>
					<?php } else { ?>
					<div class="tab-pane fade show active" id="onproses" role="tabpanel" aria-labelledby="onproses-tab">
						<ul class="main-list-ide">
							<?php 
								$args = array(
								    'orderby'       =>  'post_date',
								    'order'         =>  'DESC',
								    'posts_per_page' => 999,
								    'post_status' => array('publish', 'pending', 'private')
								);
								$query = new WP_Query($args);
								while( $query->have_posts() ): $query->the_post();
							?>
								<li>
									<div class="row">
										<div class="col-md-3">
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</div>
										<div class="col-md-2">
											By: <?php the_author_posts_link(); ?>
										</div>
										<div class="col-md-3">
											<p><?php echo bd_excerpt( 10 ); ?></p>
										</div>
										<div class="col-md-4">
											<?php if(function_exists('status_app')) status_app(get_the_ID()); ?>
										</div>
									</div>
								</li>
							<?php
								endwhile;
								wp_reset_postdata();
							 ?>
						 </ul >
					</div>
					<?php } ?>
				</div>

			</main><!-- #main -->

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>

		</div><!-- #primary -->

		<!-- Do the right sidebar check -->
		<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

			<?php get_sidebar( 'right' ); ?>

		<?php endif; ?>

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
