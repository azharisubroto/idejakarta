<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>

<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main card" id="main">

				<div class="card-body pt-5 pb-5">
					<div class="row">
						<div class="col-md-12">
							<h1 class="text-center mb-3"><?php the_title(); ?></h1>
							<div class="text-center mb-5 text-big">
								<?php while ( have_posts() ) : the_post(); ?>
									<?php echo wishbone_get_simple_likes_button( get_the_ID() ); ?>
									- Oleh <?php the_author_posts_link(); ?>
								<?php endwhile; ?>
							</div>
						</div>
						<div class="col-md-9">
							<?php while ( have_posts() ) : the_post(); ?>
								<?php
								if( has_post_thumbnail() ) { ?>
									<div class="ide-thumbnail mb-3">
										<?php the_post_thumbnail( 'full' );?>
									</div>
								<?php } ?>
								<div class="row mb-3">
									<div class="col-md-3">
										<strong>Ditulis tanggal</strong>
									</div>
									<div class="col-md-9">
										<?php the_date( '', '', '', true ); ?>
									</div>
								</div><hr>

								<div class="row mb-3">
									<div class="col-md-3">
										<strong>Klasifikasi Ide </strong>
									</div>
									<div class="col-md-9">
										<?php the_category( ', ', ''); ?>
									</div>
								</div><hr>

								<div class="row mb-3">
									<div class="col-md-3">
										<strong>Lokasi</strong>
									</div>
									<div class="col-md-9">
										<strong class="lokbantu" data-lokbantu="<?php echo get_field('lokasi_hantu' , get_the_ID()); ?>"><?php echo get_field('lokasi_hantu' , get_the_ID()); ?></strong><br>
										<br>
										<?php $location = get_field('lokasi' , get_the_ID()); ?>
										<div id="lokasifront-ide" data-location="<?php echo $location; ?>">
											<div id="locmap"></div>
										</div>
									</div>
								</div><hr>

								<div class="row mb-3">
									<div class="col-md-3">
										<strong>Latar Belakang</strong>
									</div>
									<div class="col-md-9">
										<?php echo get_field('latar_belakang'); ?>
									</div>
								</div><hr>

								<div class="row mb-3">
									<div class="col-md-3">
										<strong>Deskripsi Solusi</strong>
									</div>
									<div class="col-md-9">
										<?php the_content(); ?>
									</div>
								</div><hr>

								<div class="row mb-3">
									<div class="col-md-3">
										<strong>Dampak Ide</strong>
									</div>
									<div class="col-md-9">
										<?php echo get_field('dampak_ide'); ?>
									</div>
								</div>



								<?php // understrap_post_nav(); ?>
								<hr>
								<?php
								// If comments are open or we have at least one comment, load up the comment template.
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;
								?>

							<?php endwhile; // end of the loop. ?>
						</div>
						<div class="col-md-3">
							<div class="widget socialshare">
								<h5>Share</h5>
								<?php
								$thumb = get_post_thumbnail_id();
								$img_url = wp_get_attachment_url( $thumb );
								 ?>
								<a href="#" data-type="twitter" data-url="<?php the_permalink(); ?>" data-description="<?php echo substr(get_the_title(), 0,140); ?>" class="prettySocial btn btn-share bg-twitter"><i class="fa fa-twitter"></i> Share on Twitter</a>

								<a href="#" data-type="facebook" data-url="<?php the_permalink(); ?>" data-title="<?php the_permalink(); ?>" data-description="<?php echo get_the_excerpt(); ?>" data-media="<?php echo esc_attr($img_url); ?>" class="prettySocial btn btn-share bg-facebook"><i class="fa fa-facebook"></i> Share on Facebook</a>

								<a href="#" data-type="googleplus" data-url="<?php the_permalink(); ?>" data-description="<?php echo get_the_excerpt(); ?>" class="prettySocial btn btn-share bg-google"><i class="fa fa-google-plus"></i> Share on Google+</a>
							</div>
						</div>
					</div>
				</div>

			</main><!-- #main -->

		</div><!-- #primary -->

		<!-- Do the right sidebar check -->
		<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

			<?php get_sidebar( 'right' ); ?>

		<?php endif; ?>

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
