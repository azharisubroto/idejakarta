$(window).on('load', function(){
	var location = $('#acf-lokasi').find('#acf-field-lokasi').val();
	var location_2 = location.replace(/\s+/, "");
	var loc = location_2.split(',');

	var lokasi_alt = $('#acf-lokasi_hantu').find('#acf-field-lokasi_hantu').val();
	//alert(location);

	$('#acf-lokasi').append('<div id="lokasiide"></div>');

	if( !location ) {
		map = new GMaps({
		    div: '#lokasiide',
		    lat: -6.1753924,
		    lng: 106.82715280000002,
		    width: '100%',
		    height: '350px'
		});

		GMaps.geocode({
		  address: lokasi_alt,
		  callback: function(results, status){
		    if(status=='OK'){
		      var latlng = results[0].geometry.location;
		      var lat = latlng.lat();
		      var lng = latlng.lng();
		      map.setCenter(latlng.lat(), latlng.lng());
		      map.addMarker({
		        lat: latlng.lat(),
		        lng: latlng.lng(),
		        infoWindow: {
		          content: '<p><strong>'+ lokasi_alt +'</strong></p>'
		        }
		      });

		      $('#acf-field-lokasi').val(lat+','+lng);
		      //$('#acf-field-lokasi').attr('type', 'hidden');
		    }
		  }
		});

	} else {
		map = new GMaps({
		    div: '#lokasiide',
		    lat: loc[0],
		    lng: loc[1],
		    width: '100%',
		    height: '350px'
		});	

		map.addMarker({
		  lat: loc[0],
		  lng: loc[1],
		});
		// $('#acf-field-lokasi').attr('type', 'hidden');
	}

	$('#acf-field-lokasi_hantu').on('keyup', function(){
		var baru = $(this).val();
		map.removeMarkers();
		GMaps.geocode({
		  address: baru,
		  callback: function(results, status){
		    if(status=='OK'){
		      var latlng = results[0].geometry.location;
		      var lat = latlng.lat();
		      var lng = latlng.lng();
		      map.setCenter(latlng.lat(), latlng.lng());
		      map.addMarker({
		        lat: latlng.lat(),
		        lng: latlng.lng(),
		        infoWindow: {
		          content: '<p><strong>'+ baru +'</strong></p>'
		        }
		      });

		      $('#acf-field-lokasi').val(lat+','+lng);
		    }
		  }
		});
	});
});