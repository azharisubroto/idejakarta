$(document).ready(function($){
	$('#lokasi_hantu_33').parent('.wpuf-fields').find('.wpuf-wordlimit-message').before('<a href="#" class="carilokasi btn btn-block btn-success mt-3" id="caridong">Cari Lokasi</a>');
	$('#lokasi_hantu_33').parent('.wpuf-fields').append('<div id="lokasiide" class="mt-3"></div>');

	$('.wpuf-fields').each(function(){
		$(this).find('input[type=text]').addClass('form-control');
	});

	if( $('#lokasifront-ide').length ) {
		var location = $('#lokasifront-ide').attr('data-location');
		var location_bantu = $('strong.lokbantu').attr('data-lokbantu');
		var location_2 = location.replace(/\s+/, "");
		var loc = location_2.split(',');
		
		if( location != '' ) {
			map = new GMaps({
			    div: '#locmap',
			    lat: loc[0],
			    lng: loc[1],
			    width: '100%',
			    height: '350px'
			});	

			map.addMarker({
			  lat: loc[0],
			  lng: loc[1],
			  infoWindow: {
			    content: '<p>'+ location_bantu +'</p>'
			  }
			});
		} else {
			map = new GMaps({
			    div: '#locmap',
			    lat: -6.1753924,
			    lng: 106.82715280000002,
			    width: '100%',
			    height: '350px'
			});

			GMaps.geocode({
			  address: location_bantu,
			  callback: function(results, status){
			    if(status=='OK'){
			      var latlng = results[0].geometry.location;
			      map.setCenter(latlng.lat(), latlng.lng());
			      map.addMarker({
			        lat: latlng.lat(),
			        lng: latlng.lng(),
			        infoWindow: {
			          content: '<p><strong>'+ location_bantu +'</strong></p>'
			        }
			      });
			    }
			  }
			});
		}		
	}	

	if( $('.sl-button').length ){
		$(document).on('click', '.sl-button', function() {
			var button = $(this);
			var post_id = button.attr('data-post-id');
			var security = button.attr('data-nonce');
			var iscomment = button.attr('data-iscomment');
			var allbuttons;
			if (iscomment === '1') { /* Comments can have same id */
				allbuttons = $('.sl-comment-button-' + post_id);
			} else {
				allbuttons = $('.sl-button-' + post_id);
			}
			var loader = allbuttons.next('.sl-loader');
			if (post_id !== '') {
				$.ajax({
					type: 'POST',
					url: simpleLikes.ajaxurl,
					data: {
						action: 'wishbone_process_simple_like',
						post_id: post_id,
						nonce: security,
						is_comment: iscomment
					},
					beforeSend: function() {
						loader.html();
					},
					success: function(response) {
						var icon = response.icon;
						var count = response.count;
						allbuttons.html(icon + count);
						if (response.status === 'unliked') {
							var like_text = simpleLikes.like;
							allbuttons.prop('title', like_text);
							allbuttons.removeClass('liked');
						} else {
							var unlike_text = simpleLikes.unlike;
							allbuttons.prop('title', unlike_text);
							allbuttons.addClass('liked');
						}
						loader.empty();
					}
				});
			}
			return false;
		});

		$('.prettySocial').prettySocial();
	}
});

$(window).on('load', function(){
	map = new GMaps({
	    div: '#lokasiide',
	    lat: -12.043333,
	    lng: -77.028333,
	    width: '100%',
	    height: '350px'
	});		

	$('#caridong').on('click', function(e){
        e.preventDefault();
        GMaps.geocode({
          address: $('#lokasi_hantu_33').val().trim(),
          callback: function(results, status){
            if(status=='OK'){
              var latlng = results[0].geometry.location;
              map.setCenter(latlng.lat(), latlng.lng());
              map.addMarker({
                lat: latlng.lat(),
                lng: latlng.lng()
              });
              $('input[name=lokasi]').val(latlng.lat()+', '+latlng.lng());
            }
          }
        });
    });
});	