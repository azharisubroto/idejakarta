<?php
/**
 * The template for displaying the author pages.
 *
 * Learn more: https://codex.wordpress.org/Author_Templates
 *
 * @package understrap
 */

get_header();
$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );
?>


<div class="wrapper" id="author-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<div class="tab-content card" id="myTabContent">
					<div class="tab-pane fade show active" id="onproses" role="tabpanel" aria-labelledby="onproses-tab">
						<div class="card-body pb-0">
							<?php
							$curauth = ( isset( $_GET['author_name'] ) ) ? get_user_by( 'slug',
								$author_name ) : get_userdata( intval( $author ) );
							?>

							<h4><?php esc_html_e( 'Ide Oleh: ', 'understrap' ); ?><?php echo esc_html( $curauth->nickname ); ?></h4>
							<hr>
						</div>
						<ul class="main-list-ide">
							<?php 
								while( have_posts() ): the_post();
							?>
								<li>
									<div class="row">
										<div class="col-md-3">
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</div>
										<div class="col-md-2">
											By: <?php the_author_posts_link(); ?>
										</div>
										<div class="col-md-3">
											<p><?php echo bd_excerpt( 10 ); ?></p>
										</div>
										<div class="col-md-4">
											<?php if(function_exists('status_app')) status_app(get_the_ID()); ?>
										</div>
									</div>
								</li>
							<?php
								endwhile;
								wp_reset_postdata();
							 ?>
						 </ul >
					</div>
				</div>

				</ul>

			</main><!-- #main -->

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>

		</div><!-- #primary -->

		<!-- Do the right sidebar check -->
		<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

			<?php get_sidebar( 'right' ); ?>

		<?php endif; ?>

	</div> <!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
