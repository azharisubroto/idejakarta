<?php
/**
* Template Name: Lokasi
*/

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">

				<main class="site-main card" id="main" role="main">
					
					<div class="card-body">
						<h1 class="entry-title"><?php the_title(); ?></h1>

						<div id="ideamap"></div>

						<script type="text/javascript">
							    $(document).ready(function(){
							      map = new GMaps({
							        div: '#ideamap',
							        lat: -6.1753924,
							        lng: 106.82715280000002,
							        width: '100%',
							        height: '550px',
							        zoom: 11
							      });

							      var path = [
							      				[-6.012874,106.680679],
							      				[-5.937753,106.98967],
							      				[-5.9719,107.017822],
							      				[-5.98829,107.012329],
							      				[-6.000582,107.006836],
							      				[-6.004679,106.999283],
							      				[-6.012874,106.99173],
							      				[-6.025165,106.995163],
							      				[-6.024482,107.010269],
							      				[-6.030628,107.009583],
							      				[-6.037456,107.008209],
							      				[-6.04565,107.007523],
							      				[-6.046333,106.977997],
							      				[-6.0675,106.999969],
							      				[-6.076377,107.013702],
							      				[-6.085936,106.986923],
							      				[-6.091398,106.97113],
							      				[-6.208138,106.964264],
							      				[-6.228617,106.947098],
							      				[-6.251142,106.943665],
							      				[-6.253189,106.919632],
							      				[-6.267523,106.906586],
							      				[-6.286634,106.910706],
							      				[-6.304379,106.922379],
							      				[-6.350104,106.917572],
							      				[-6.363752,106.914825],
							      				[-6.368529,106.898346],
							      				[-6.36034,106.875],
							      				[-6.355563,106.8647],
							      				[-6.346009,106.855774],
							      				[-6.337137,106.846848],
							      				[-6.343962,106.838608],
							      				[-6.353516,106.835175],
							      				[-6.356928,106.821442],
							      				[-6.36307,106.812515],
							      				[-6.364435,106.797409],
							      				[-6.331678,106.801529],
							      				[-6.315299,106.782303],
							      				[-6.299601,106.77269],
							      				[-6.276396,106.75621],
							      				[-6.246856,106.747779],
							      				[-6.226569,106.738358],
							      				[-6.224521,106.719131],
							      				[-6.193803,106.724625],
							      				[-6.187659,106.717072],
							      				[-6.170593,106.688232],
							      				[-6.142603,106.688232],
							      				[-6.137141,106.696472],
							      				[-6.12417,106.686859],
							      				[-6.110515,106.697159],
							      				[-6.097543,106.686172],
							      				[-6.094812,106.704025],
							      				[-6.090032,106.722565],
							      				[-6.056575,106.711578],
							      				[-6.026531,106.702652],
							      				[-6.013737,106.682068]
							      			];
							      	polygon = map.drawPolygon({
							      	  paths: path, // pre-defined polygon shape
							      	  strokeColor: 'red',
							      	  strokeOpacity: .3,
							      	  strokeWeight: 3,
							      	  fillColor: 'yellow',
							      	  fillOpacity: 0.1
							      	});

							      <?php 
							      	$args = array(
  									    'orderby'       =>  'post_date',
  									    'order'         =>  'DESC',
  									    'posts_per_page' => 999,
  									    'post_status' => array('publish', 'pending', 'private')
  									);
  									$query = new WP_Query($args);
  									while( $query->have_posts() ): $query->the_post();
  									$location = get_field('lokasi' , get_the_ID());
  									$location_label = get_field('lokasi_hantu' , get_the_ID());
  									if( isset($location) ) {
  										$loc = preg_replace('/\s+/', ' ', $location);
  										$loc = explode(',', $loc);

  										if( '' !== $loc[0] && '' !== $loc[1] ) {
  											$lat = $loc[0];
  											$lng = $loc[1];
  									?>
  										map.addMarker({
  										  lat: <?php echo $lat; ?>,
  										  lng: <?php echo $lng; ?>,
  										  title: '<?php the_title(); ?>',
  										  infoWindow: {
  										  	maxWidth: 400,
  										    content: '<h6><?php the_title(); ?></h6><p><?php echo bd_excerpt( 20 ); ?></p><p>Lokasi: <?php echo $location_label; ?></p><p><a href="<?php the_permalink();?>">Baca Selengkapnya &raquo;</a></p>'
  										  }
  										});
  									<?php
  										} 
  									}
							       ?>
							      
							      <?php endwhile; wp_reset_postdata(); ?>
							    });
						</script>
					</div>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
