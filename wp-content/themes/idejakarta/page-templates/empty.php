<?php
/**
 * Template Name: Empty Page Template
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

?>
	<div class="wrapper" id="full-width-page-wrapper">

		<div class="<?php echo esc_attr( $container ); ?>" id="content">

			<div class="row">

				<div class="col-md-12 content-area" id="primary">

					<main class="site-main" id="main" role="main">
						<ol>

						<?php 
							$args = array(
							    'author'        =>  get_current_user_id(),
							    'orderby'       =>  'post_date',
							    'order'         =>  'ASC',
							    'posts_per_page' => 10,
							    'post_status' => array('publish', 'pending', 'private')
							    );
							$query = new WP_Query($args);
							while( $query->have_posts() ): $query->the_post();
						?>
							<li>
								<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
								<div class="clearfix"></div>
								Status: <?php the_field( "progress", get_the_ID() ); ?> <br>
								Approval: <?php the_field( "approval", get_the_ID() ); ?>
							</li>
						<?php
							endwhile;
							wp_reset_postdata();
						 ?>
						 </ol>

					</main><!-- #main -->

				</div><!-- #primary -->

			</div><!-- .row end -->

		</div><!-- Container end -->

	</div><!-- Wrapper end -->
<?php

get_footer();
