<?php
/**
 * Template Name: Daftar Ide
 *
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">

				<main class="site-main" id="main" role="main">
					<ol>

					<?php 
						$args = array(
						    'author'        =>  $current_user->ID,
						    'orderby'       =>  'post_date',
						    'order'         =>  'ASC',
						    'posts_per_page' => 1
						    );
						$query = new WP_Query($args);
						while( $query->have_posts() ) $query->the_post();
					?>
						<li>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<div class="clearfix"></div>
							Status: <?php echo get_field( "progress" ); ?> <br>
							Approval: <?php echo get_field( "approval" ); ?>
						</li>
					<?php
						endwhile();
						wp_reset_postdata();
					 ?>
					 </ol>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
